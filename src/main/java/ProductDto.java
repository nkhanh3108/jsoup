import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)

public class ProductDto {
private String doccumentno;
private String docstatus;
private Float totalpayment;
private String userbuy;
private String receivename;
private String receivephone;
private String receiveadd;
private BigDecimal productId;
private String sku;
private String certificateFile;
private String upc;
private String name;
private BigDecimal qty;
private BigDecimal price;
private BigDecimal uomId;
private BigDecimal guaranteedays = BigDecimal.ZERO;
private BigDecimal guaranteedaysmin = BigDecimal.ZERO;
private String descriptionUrl;
private BigDecimal imageId;
private String productType;
private String description;
private String isPricePrinted;
private BigDecimal brandId;
private String returnable;
private BigDecimal overdueReturnDays;
private BigDecimal stockMin;
private BigDecimal volume;
private BigDecimal weight;
private BigDecimal shelfWidth;
private BigDecimal shelfHeight;
private BigDecimal shelfDepth;
private String documentNote;
private String value;
private String productUu;
private BigDecimal salesRepId;
private String isStocked;
private String isSold;
private String isActive;
private BigDecimal productCategoryId;
private String productCategoryName;
private BigDecimal taxCategoryId;
private String downloadUrl;
private BigDecimal locatorId;
private BigDecimal attributeSetInstanceId;
private String imageThumbnail;
private BigDecimal fomula;
private String showText;
private String currencyCode;
private BigDecimal priceNew;
private String isSelling;
private String isHasCounter;
private BigDecimal serviceValue;
private String fromDate;
private String toDate;
private String type;
private String refundPolicy;

    public String getDoccumentno() {
        return doccumentno;
    }

    public void setDoccumentno(String doccumentno) {
        this.doccumentno = doccumentno;
    }

    public String getDocstatus() {
        return docstatus;
    }

    public void setDocstatus(String docstatus) {
        this.docstatus = docstatus;
    }

    public Float getTotalpayment() {
        return totalpayment;
    }

    public void setTotalpayment(Float totalpayment) {
        this.totalpayment = totalpayment;
    }

    public String getUserbuy() {
        return userbuy;
    }

    public void setUserbuy(String userbuy) {
        this.userbuy = userbuy;
    }

    public String getReceivename() {
        return receivename;
    }

    public void setReceivename(String receivename) {
        this.receivename = receivename;
    }

    public String getReceivephone() {
        return receivephone;
    }

    public void setReceivephone(String receivephone) {
        this.receivephone = receivephone;
    }

    public String getReceiveadd() {
        return receiveadd;
    }

    public void setReceiveadd(String receiveadd) {
        this.receiveadd = receiveadd;
    }

    public BigDecimal getProductId() {
        return productId;
    }

    public void setProductId(BigDecimal productId) {
        this.productId = productId;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getCertificateFile() {
        return certificateFile;
    }

    public void setCertificateFile(String certificateFile) {
        this.certificateFile = certificateFile;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getUomId() {
        return uomId;
    }

    public void setUomId(BigDecimal uomId) {
        this.uomId = uomId;
    }

    public BigDecimal getGuaranteedays() {
        return guaranteedays;
    }

    public void setGuaranteedays(BigDecimal guaranteedays) {
        this.guaranteedays = guaranteedays;
    }

    public BigDecimal getGuaranteedaysmin() {
        return guaranteedaysmin;
    }

    public void setGuaranteedaysmin(BigDecimal guaranteedaysmin) {
        this.guaranteedaysmin = guaranteedaysmin;
    }

    public String getDescriptionUrl() {
        return descriptionUrl;
    }

    public void setDescriptionUrl(String descriptionUrl) {
        this.descriptionUrl = descriptionUrl;
    }

    public BigDecimal getImageId() {
        return imageId;
    }

    public void setImageId(BigDecimal imageId) {
        this.imageId = imageId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsPricePrinted() {
        return isPricePrinted;
    }

    public void setIsPricePrinted(String isPricePrinted) {
        this.isPricePrinted = isPricePrinted;
    }

    public BigDecimal getBrandId() {
        return brandId;
    }

    public void setBrandId(BigDecimal brandId) {
        this.brandId = brandId;
    }

    public String getReturnable() {
        return returnable;
    }

    public void setReturnable(String returnable) {
        this.returnable = returnable;
    }

    public BigDecimal getOverdueReturnDays() {
        return overdueReturnDays;
    }

    public void setOverdueReturnDays(BigDecimal overdueReturnDays) {
        this.overdueReturnDays = overdueReturnDays;
    }

    public BigDecimal getStockMin() {
        return stockMin;
    }

    public void setStockMin(BigDecimal stockMin) {
        this.stockMin = stockMin;
    }

    public BigDecimal getVolume() {
        return volume;
    }

    public void setVolume(BigDecimal volume) {
        this.volume = volume;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getShelfWidth() {
        return shelfWidth;
    }

    public void setShelfWidth(BigDecimal shelfWidth) {
        this.shelfWidth = shelfWidth;
    }

    public BigDecimal getShelfHeight() {
        return shelfHeight;
    }

    public void setShelfHeight(BigDecimal shelfHeight) {
        this.shelfHeight = shelfHeight;
    }

    public BigDecimal getShelfDepth() {
        return shelfDepth;
    }

    public void setShelfDepth(BigDecimal shelfDepth) {
        this.shelfDepth = shelfDepth;
    }

    public String getDocumentNote() {
        return documentNote;
    }

    public void setDocumentNote(String documentNote) {
        this.documentNote = documentNote;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getProductUu() {
        return productUu;
    }

    public void setProductUu(String productUu) {
        this.productUu = productUu;
    }

    public BigDecimal getSalesRepId() {
        return salesRepId;
    }

    public void setSalesRepId(BigDecimal salesRepId) {
        this.salesRepId = salesRepId;
    }

    public String getIsStocked() {
        return isStocked;
    }

    public void setIsStocked(String isStocked) {
        this.isStocked = isStocked;
    }

    public String getIsSold() {
        return isSold;
    }

    public void setIsSold(String isSold) {
        this.isSold = isSold;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public BigDecimal getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(BigDecimal productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public String getProductCategoryName() {
        return productCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        this.productCategoryName = productCategoryName;
    }

    public BigDecimal getTaxCategoryId() {
        return taxCategoryId;
    }

    public void setTaxCategoryId(BigDecimal taxCategoryId) {
        this.taxCategoryId = taxCategoryId;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public BigDecimal getLocatorId() {
        return locatorId;
    }

    public void setLocatorId(BigDecimal locatorId) {
        this.locatorId = locatorId;
    }

    public BigDecimal getAttributeSetInstanceId() {
        return attributeSetInstanceId;
    }

    public void setAttributeSetInstanceId(BigDecimal attributeSetInstanceId) {
        this.attributeSetInstanceId = attributeSetInstanceId;
    }

    public String getImageThumbnail() {
        return imageThumbnail;
    }

    public void setImageThumbnail(String imageThumbnail) {
        this.imageThumbnail = imageThumbnail;
    }

    public BigDecimal getFomula() {
        return fomula;
    }

    public void setFomula(BigDecimal fomula) {
        this.fomula = fomula;
    }

    public String getShowText() {
        return showText;
    }

    public void setShowText(String showText) {
        this.showText = showText;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getPriceNew() {
        return priceNew;
    }

    public void setPriceNew(BigDecimal priceNew) {
        this.priceNew = priceNew;
    }

    public String getIsSelling() {
        return isSelling;
    }

    public void setIsSelling(String isSelling) {
        this.isSelling = isSelling;
    }

    public String getIsHasCounter() {
        return isHasCounter;
    }

    public void setIsHasCounter(String isHasCounter) {
        this.isHasCounter = isHasCounter;
    }

    public BigDecimal getServiceValue() {
        return serviceValue;
    }

    public void setServiceValue(BigDecimal serviceValue) {
        this.serviceValue = serviceValue;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRefundPolicy() {
        return refundPolicy;
    }

    public void setRefundPolicy(String refundPolicy) {
        this.refundPolicy = refundPolicy;
    }
}