import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductImageDto {

    private BigDecimal productImageId;
    private String alternateText;
    private String description;
    private String internalImagePath;
    private String name;
    private BigDecimal productId;
    private String productImageUu;
    private MultipartFile multipartFile;

    public BigDecimal getProductImageId() {
        return productImageId;
    }

    public void setProductImageId(BigDecimal productImageId) {
        this.productImageId = productImageId;
    }

    public String getAlternateText() {
        return alternateText;
    }

    public void setAlternateText(String alternateText) {
        this.alternateText = alternateText;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInternalImagePath() {
        return internalImagePath;
    }

    public void setInternalImagePath(String internalImagePath) {
        this.internalImagePath = internalImagePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getProductId() {
        return productId;
    }

    public void setProductId(BigDecimal productId) {
        this.productId = productId;
    }

    public String getProductImageUu() {
        return productImageUu;
    }

    public void setProductImageUu(String productImageUu) {
        this.productImageUu = productImageUu;
    }

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }
}

