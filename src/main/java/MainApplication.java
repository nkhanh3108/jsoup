import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;
import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptJobManager;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URL;
import java.sql.Array;
import java.sql.SQLOutput;
import java.util.*;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxDriverLogLevel;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;
import org.w3c.dom.html.HTMLElement;


public class MainApplication {
    @Autowired
    private static RestTemplate restTemplate;

    public static void main(String[] args) throws IOException {
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MDY1Mjc1MzEsInVzZXIiOnsiY2xpZW50SWQiOjUsImZ1bGxOYW1lIjpudWxsLCJ1c2VySWQiOjI1LCJlbWFpbCI6ImFkbWluX29yZ0BnbWFpbC5jb20iLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOSVNUUkFUT1IiLCJQX01FRElBX0MiLCJQX1BST0RVQ1RfRCIsIlBfTUVESUFfUiIsIlBfUFJPRFVDVF9DIiwiUF9RVUVTVElPTl9EIiwiUF9ESVNDT1VOVF9VIiwiUF9ESVNDT1VOVF9EIiwiUF9PUkdfQkFOS19SIiwiUF9DTElFTlRfSU5WT0lDRSIsIlBfQ0hBUlRfTU9OVEgiLCJQX0NMSUVOVF9DQVRFR09SWSIsIlBfU0hPUF9TSElQUEVSX0QiLCJQX09SR19JTlZPSUNFX0MiLCJQX1dBTExFVF9VIiwiUF9TSE9QX1NISVBQRVJfQyIsIlBfQ09NTUVOVF9SIiwiUF9DTElFTlRfQkFOS19MIiwiUF9GSU5BTkNFX1VTRVIiLCJQX0JBTk5FUiIsIlBfV0FMTEVUX0MiLCJQX0JSQU5EIiwiUF9GSU5BTkNFX1IiLCJQX1dBTExFVF9SIiwiUF9QQVJUTkVSIiwiUF9DTElFTlRfQkFOS19DIiwiUF9GSU5BTkNFX0MiLCJQX0RJU0NPVU5UX1IiLCJQX0FSVElDTEVfUiIsIlBfRklOQU5DRV9NIiwiUF9BUlRJQ0xFX0MiLCJQX09SR19CQU5LX1UiLCJQX09XTkVSIiwiUF9PUkdfSU5WT0lDRV9VIiwiUF9PUkdfQkFOS19DIiwiUF9XQUxMRVRfRCIsIlBfUFJPRFVDVF9VIiwiUF9PUkdfSU5WT0lDRSIsIlBfUk9MRV9EIiwiUF9PUkdfQkFOS19MIiwiUF9DTElFTlRfQkFOS19VIiwiUF9RVUVTVElPTl9DIiwiUF9DTElFTlRfQkFOS19EIiwiUF9PUkRFUl9SIiwiUF9DT01QQVJFX0RFTElWRVJZIiwiUF9RVUVTVElPTl9VIiwiUF9ST0xFX1IiLCJQX09SR19CQU5LIiwiUF9PUkdfSU5WT0lDRV9SIiwiUF9PUkdfQkFOS19EIiwiUF9DTElFTlRfQkFOSyIsIlBfQ0xJRU5UX0JBTktfUiIsIlBfU0hPUF9TSElQUEVSX1IiLCJQX1VTRVJfR1JPVVBfQSIsIlBfQ0xJRU5UX0lOVk9JQ0VfRCIsIlBfT1JERVJfQyIsIlBfTUVESUFfRCIsIlBfUk9MRV9DIiwiUF9BRE1JTiIsIlBfQ09NUEFSRV9VU0VSIiwiUF9DSEFSVF9QUk9EVUNUIiwiUF9DT01QQVJFX0RJU1RSSUJVVE9SIiwiUF9SRVBPUlRfUFJPRFVDVCIsIlBfRklOQU5DRV9EIiwiUF9QUk9EVUNUX0EiLCJQX09SREVSX0QiLCJQX0NIQVQiLCJQX01FRElBX1UiLCJQX1FVRVNUSU9OX0NBVEVHT1JZX1UiLCJQX1VTRVJfUiIsIlBfT1JHX0lOVk9JQ0VfRCIsIlBfT1JERVJfVSIsIlBfRklOQU5DRV9VIiwiUF9SRVBPUlRfU0hPUCIsIlBfT1JHX0lOVk9JQ0VfTCIsIlBfVVNFUl9BIiwiUF9PUkRFUl9SRSIsIlBfQ0xJRU5UX0lOVk9JQ0VfUiIsIlBfQ0hBUlRfT1JERVIiLCJQX0NMSUVOVF9JTlZPSUNFX1UiLCJQX1NFVFRJTkciLCJQX1FVRVNUSU9OX0NBVEVHT1JZX0MiLCJQX0NNUyIsIlBfQ09NUEFSRV9TRUxMRVIiLCJQX1VTRVJfRCIsIlBfRklOQU5DRV9DX0NMSUVOVCIsIlBfQ09NTUVOVF9BIiwiUF9ESVNDT1VOVF9DIiwiUF9PUkRFUl9SRiIsIlBfRklOQU5DRV9NQU5BR0UiLCJQX09SREVSX0EiLCJQX09SRyIsIlBfVVNFUl9DIiwiUF9DTElFTlRfSU5WT0lDRV9DIiwiUF9ST0xFX1UiLCJQX0NMSUVOVF9VIiwiUF9DTElFTlRfSU5WT0lDRV9MIiwiUF9RVUVTVElPTl9DQVRFR09SWV9EIiwiUF9VU0VSX1UiLCJQX1VTRVJfR1JPVVBfQyIsIlBfQVJUSUNMRV9VIiwiUF9DTElFTlQiLCJQX0FSVElDTEVfRCIsIlBfUFJPRFVDVF9SIiwiUk9MRV9CVVlFUiIsIlBfQ09NTUVOVF9DIl0sIm9yZ0lkIjoyMSwidXNlcm5hbWUiOiJwX29yZ0BnbWFpbC5jb20iLCJpc0FwcHJvdmUiOiJZIn19.h-bdBjfFKsckmTEexz_4ujsKfi-SB4JIEjt5sWcqU5M";

      /*  phuminh(token,"https://hangnhapkhauthailan.com/danh-muc-san-pham/sua-tam-sua-rua-mat-67", 440, 13715);*/

       gomsuminhlong(token,
                "http://smartstores.qtq.vn/", 440, 13715);


    }

    private static void denanphuoc(String token, String s, int cate, int brand) {

        restTemplate = new RestTemplate();
        List<String> urls = new ArrayList<>();
        urls.add(s);
        for (String url : urls
        ) {
            System.out.println("Fetching %s..." + url);
            try {
                Document doc = Jsoup.connect(url).get();
                Elements elements = doc.select("div .col-inner");
                System.out.println(elements);
                for (Element e : elements
                ) {
                    Element name = e.getElementsByClass("name").first();
                    Element price = e.getElementsByClass("woocommerce-Price-amount").first();
                    Element disPrice = e.getElementsByClass("woocommerce-Price-amount").last();
                    Element link = e.select("a[href]").first(); // a with href
                    if (name != null && disPrice != null && link != null) {
                        Document docDetail = Jsoup.connect(link.attr("href")).get();
                        Element description = docDetail.getElementsByClass("product-short-description").first();
                        Element longdescription = docDetail.getElementsByClass("woocommerce-Tabs-panel--description").first();
                        Element imageThumbnail = docDetail.getElementsByClass("wp-post-image").first();
                        String imageThumb = getImage(imageThumbnail.absUrl("src"));
                        Elements images = docDetail.getElementsByClass("attachment-woocommerce_thumbnail");
                        List<String> imagesName = new ArrayList<>();
                        for (Element ele : images) {
                            imagesName.add(getImage(ele.absUrl("src")));
                        }

                        ProductModel productModel = new ProductModel();
                        // Product Dto
                        ProductDto productDto = new ProductDto();
                        productDto.setName(name.text());
                        productDto.setSku(null);
                        if (longdescription != null) {
                            productDto.setDescription(longdescription.text());
                        }


                        productDto.setProductCategoryId(new BigDecimal(cate));
                        productDto.setBrandId(new BigDecimal(brand));


                        productDto.setCurrencyCode("VND");
                        productDto.setPrice(new BigDecimal(getPrice(disPrice.text())));
                        productDto.setFomula(new BigDecimal(1));
                        productDto.setQty(new BigDecimal(10));
                        productDto.setWeight(new BigDecimal(1));
                        productDto.setShelfWidth(new BigDecimal(1));
                        productDto.setShelfDepth(new BigDecimal(1));
                        productDto.setShelfHeight(new BigDecimal(1));
                        productDto.setImageThumbnail(imageThumb);
                        productModel.setProductDto(productDto);
                        // Product Carrier
                        List<ProductCarrierDto> productCarrierDtos = new ArrayList<>();
                        ProductCarrierDto productCarrierDto1 = new ProductCarrierDto();

                        productCarrierDto1.setCarrierId(new BigDecimal(3));
                        productCarrierDto1.setCarrierName("Giao Hàng Tiết Kiệm");
                        productCarrierDtos.add(productCarrierDto1);


                        ProductCarrierDto productCarrierDto2 = new ProductCarrierDto();
                        productCarrierDto2.setCarrierId(new BigDecimal(1));
                        productCarrierDto2.setCarrierName("Giao Hàng EShip");
                        productCarrierDtos.add(productCarrierDto2);

                        ProductCarrierDto productCarrierDto3 = new ProductCarrierDto();
                        productCarrierDto3.setCarrierId(new BigDecimal(2));
                        productCarrierDto3.setCarrierName("Giao Hàng Nhanh");
                        productCarrierDtos.add(productCarrierDto3);

                        // Product Image
                        List<ProductImageDto> productImageDtos = new ArrayList<>();
                        for (int i = 0; i < imagesName.size(); i++) {
                            ProductImageDto productImageDto = new ProductImageDto();
                            productImageDto.setName(imagesName.get(i));
                            productImageDtos.add(productImageDto);
                        }


                        productModel.setProductDto(productDto);
                        productModel.setProductCarrierDtos(productCarrierDtos);
                        productModel.setProductImageDtos(productImageDtos);
                        HttpHeaders headers = new HttpHeaders();
                        headers.setContentType(MediaType.APPLICATION_JSON);
                        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
                        headers.set("ACCESS_TOKEN", "Bearer " + token);
                        HttpEntity<ProductModel> entity = new HttpEntity<>(productModel, headers);
                        ResponseEntity<ServiceResult> forEntity = restTemplate.postForEntity("http://103.226.248.230:8889/api/product/create", entity,
                                ServiceResult.class);
                        if (forEntity.getStatusCode() == HttpStatus.CREATED) {
                            System.out.println("Success ----------- " + name.text());
                        } else {
                            System.out.println("FAIL ----------- " + name.text());
                        }

                    }

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private static void jewel(String token, String s, int cate, int brand) {

        restTemplate = new RestTemplate();
        List<String> urls = new ArrayList<>();
        urls.add(s);
        for (String url : urls
        ) {
            System.out.println("Fetching %s..." + url);
            try {
                Document doc = Jsoup.connect(url).get();
                Elements elements = doc.select("div .product-box");
                for (Element e : elements
                ) {
                    Element name = e.getElementsByClass("product-name").first();
                    Element disPrice = e.getElementsByClass("product-price").first();
                    Element link = e.select("a[href]").first(); // a with href
                    if (name != null && link != null) {
                        Document docDetail = Jsoup.connect(link.absUrl("href")).get();
                        Element longdescription = docDetail.getElementsByClass("rte").first();
                        Element imageThumbnail = e.select("img").first();
                        String imageThumb = getImage(imageThumbnail.absUrl("data-lazyload"));
                        Elements images = docDetail.getElementsByClass("thumb-link").select("img");

                        List<String> imagesName = new ArrayList<>();
                        if (images != null) {
                            for (Element ele : images) {
                                imagesName.add(getImage(ele.absUrl("src")));
                            }
                        }
                        ProductModel productModel = new ProductModel();
                        // Product Dto
                        ProductDto productDto = new ProductDto();
                        productDto.setName(name.text());
                        productDto.setSku(null);
                        if (longdescription != null) {
                            productDto.setDescription(longdescription.text());
                        }


                        productDto.setProductCategoryId(new BigDecimal(cate));
                        productDto.setBrandId(new BigDecimal(brand));


                        productDto.setCurrencyCode("VND");
                        productDto.setPrice(new BigDecimal(getPrice(disPrice.text())));
                        productDto.setFomula(new BigDecimal(1));
                        productDto.setQty(new BigDecimal(10));
                        productDto.setWeight(new BigDecimal(1));
                        productDto.setShelfWidth(new BigDecimal(1));
                        productDto.setShelfDepth(new BigDecimal(1));
                        productDto.setShelfHeight(new BigDecimal(1));
                        productDto.setImageThumbnail(imageThumb);
                        productModel.setProductDto(productDto);
                        // Product Carrier
                        List<ProductCarrierDto> productCarrierDtos = new ArrayList<>();
                        ProductCarrierDto productCarrierDto1 = new ProductCarrierDto();

                        productCarrierDto1.setCarrierId(new BigDecimal(3));
                        productCarrierDto1.setCarrierName("Giao Hàng Tiết Kiệm");
                        productCarrierDtos.add(productCarrierDto1);


                        ProductCarrierDto productCarrierDto2 = new ProductCarrierDto();
                        productCarrierDto2.setCarrierId(new BigDecimal(1));
                        productCarrierDto2.setCarrierName("Giao Hàng EShip");
                        productCarrierDtos.add(productCarrierDto2);

                        ProductCarrierDto productCarrierDto3 = new ProductCarrierDto();
                        productCarrierDto3.setCarrierId(new BigDecimal(2));
                        productCarrierDto3.setCarrierName("Giao Hàng Nhanh");
                        productCarrierDtos.add(productCarrierDto3);

                        // Product Image
                        List<ProductImageDto> productImageDtos = new ArrayList<>();
                        for (int i = 0; i < imagesName.size(); i++) {
                            ProductImageDto productImageDto = new ProductImageDto();
                            productImageDto.setName(imagesName.get(i));
                            productImageDtos.add(productImageDto);
                        }


                        productModel.setProductDto(productDto);
                        productModel.setProductCarrierDtos(productCarrierDtos);
                        productModel.setProductImageDtos(productImageDtos);
                        HttpHeaders headers = new HttpHeaders();
                        headers.setContentType(MediaType.APPLICATION_JSON);
                        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
                        headers.set("ACCESS_TOKEN", "Bearer " + token);
                        HttpEntity<ProductModel> entity = new HttpEntity<>(productModel, headers);
                        ResponseEntity<ServiceResult> forEntity = restTemplate.postForEntity("http://103.226.248.230:8889/api/product/create", entity,
                                ServiceResult.class);
                        if (forEntity.getStatusCode() == HttpStatus.CREATED) {
                            System.out.println("Success ----------- " + name.text());
                        } else {
                            System.out.println("FAIL ----------- " + name.text());
                        }

                    }

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    //test
  /*  private static void gomsuminhlong(String token, String s, int cate, int brand) {
        restTemplate  = new RestTemplate();
        List<String> urls = new ArrayList<>();
        urls.add(s);
        for(String url : urls){
                    WebClient client =  new WebClient(BrowserVersion.CHROME);
                    client.getOptions().setJavaScriptEnabled(false);
                    client.getOptions().setCssEnabled(false);
                    client.getOptions().setUseInsecureSSL(true);
                    try{
                        HtmlPage page = client.getPage(url);
                        //div tổng
                        List<HtmlElement> items =   page.getByXPath("//div[@class ='w30s-repeater-item']");
                        System.out.println("aaa"+ items);
                        if(items.isEmpty()){
                            System.out.println("No items found");
                        }
                        else{
                            for(HtmlElement htmlItem : items){
                                HtmlAnchor href = htmlItem.getFirstByXPath(".//a");
                                Element price = htmlItem.getFirstByXPath("");
                                if(href != null ){
                                    HtmlPage docDetail = client.getPage(href.getAttribute("href"));
                                    DomNodeList<HtmlElement> images = docDetail.getElementById("gallery_01").getElementsByTagName("img");
                                    List<String> imagesName = new ArrayList<>();
                                    if (images != null) {
                                        for (HtmlElement ele : images) {
                                            imagesName.add(getImage(ele.getAttribute("src")));
                                        }
                                    }
                                }


                                ProductDto itemDto = new ProductDto();
                                itemDto.setName(href.asText());
                                ObjectMapper mapper = new ObjectMapper();
                                String JsonString = mapper.writeValueAsString(itemDto);
                                System.out.println("aaaa"+ JsonString);



                            }
                        }


                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }

                }




    }*/

    //end

 private static void gomsuminhlong(String token, String s, int cate, int brand) {

        restTemplate = new RestTemplate();
        List<String> urls = new ArrayList<>();
        urls.add(s);
        for (String url : urls
        ) {
            System.out.println("Fetching %s..." + url);
            try {
//                WebClient client = new WebClient(BrowserVersion.FIREFOX_68);
//
//                client.getOptions().setThrowExceptionOnScriptError(false); //even if there is error in js continue
//                client.getOptions().setThrowExceptionOnFailingStatusCode(false);
//                client.getOptions().setCssEnabled(false);
//                client.getOptions().setJavaScriptEnabled(true);
//
//                client.waitForBackgroundJavaScript(300000);
//                client.waitForBackgroundJavaScriptStartingBefore(300000000);
//
//                HtmlPage page = client.getPage(url);
//
//                page.asXml();
//                List<Object> byXPath = page.getByXPath("//div[@class='w30s-overlay']");
                System.setProperty("webdriver.chrome.driver","C:\\Users\\ADMIN\\Downloads\\chromedriver_win32 (1)\\chromedriver.exe");
                WebDriver driver = new ChromeDriver();
                driver.get(url);

                Document doc = Jsoup.connect(url).get();
                Elements elements = doc.select("div .w30s-repeater-box");
                System.out.println(elements);
                for (Element e : elements
                ) {
                    Element name = e.getElementsByClass("w30s-content").first();
                    Element disPrice = e.getElementsByClass("w30s-widget-631165").last();
                    Element link = e.select("a[href]").first(); // a with href
                    if (name != null && link != null) {
                        Document docDetail = Jsoup.connect(link.attr("href")).get();
                        Element longdescription = docDetail.getElementsByClass("w30s-widget-631235").first();
                        Element imageThumbnail = docDetail.getElementsByClass("w30s-overlay-631206").first();
                        String imageThumb = getImage(imageThumbnail.absUrl("src"));
                        Elements images = docDetail.getElementsByClass("w30s-overlay-631206");
                        List<String> imagesName = new ArrayList<>();
                        for (Element ele : images) {
                            imagesName.add(getImage(ele.absUrl("src")));
                        }

                        ProductModel productModel = new ProductModel();
                        // Product Dto
                        ProductDto productDto = new ProductDto();
                        productDto.setName(name.text());
                        productDto.setSku(null);
                        if (longdescription != null) {
                            productDto.setDescription(longdescription.text());
                        }


                        productDto.setProductCategoryId(new BigDecimal(cate));
                        productDto.setBrandId(new BigDecimal(brand));


                        productDto.setCurrencyCode("VND");
                        productDto.setPrice(new BigDecimal(getPrice(disPrice.text())));
                        productDto.setFomula(new BigDecimal(1));
                        productDto.setQty(new BigDecimal(10));
                        productDto.setWeight(new BigDecimal(1));
                        productDto.setShelfWidth(new BigDecimal(1));
                        productDto.setShelfDepth(new BigDecimal(1));
                        productDto.setShelfHeight(new BigDecimal(1));
                        productDto.setImageThumbnail(imageThumb);
                        productModel.setProductDto(productDto);
                        // Product Carrier
                        List<ProductCarrierDto> productCarrierDtos = new ArrayList<>();
                        ProductCarrierDto productCarrierDto1 = new ProductCarrierDto();

                        productCarrierDto1.setCarrierId(new BigDecimal(3));
                        productCarrierDto1.setCarrierName("Giao Hàng Tiết Kiệm");
                        productCarrierDtos.add(productCarrierDto1);


                        ProductCarrierDto productCarrierDto2 = new ProductCarrierDto();
                        productCarrierDto2.setCarrierId(new BigDecimal(1));
                        productCarrierDto2.setCarrierName("Giao Hàng EShip");
                        productCarrierDtos.add(productCarrierDto2);

                        ProductCarrierDto productCarrierDto3 = new ProductCarrierDto();
                        productCarrierDto3.setCarrierId(new BigDecimal(2));
                        productCarrierDto3.setCarrierName("Giao Hàng Nhanh");
                        productCarrierDtos.add(productCarrierDto3);

                        // Product Image
                        List<ProductImageDto> productImageDtos = new ArrayList<>();
                        for (int i = 0; i < imagesName.size(); i++) {
                            ProductImageDto productImageDto = new ProductImageDto();
                            productImageDto.setName(imagesName.get(i));
                            productImageDtos.add(productImageDto);
                        }


                        productModel.setProductDto(productDto);
                        productModel.setProductCarrierDtos(productCarrierDtos);
                        productModel.setProductImageDtos(productImageDtos);
                        HttpHeaders headers = new HttpHeaders();
                        headers.setContentType(MediaType.APPLICATION_JSON);
                        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
                        headers.set("ACCESS_TOKEN", "Bearer " + token);
                        HttpEntity<ProductModel> entity = new HttpEntity<>(productModel, headers);
                        ResponseEntity<ServiceResult> forEntity = restTemplate.postForEntity("http://103.226.248.230:8889/api/product/create", entity,
                                ServiceResult.class);
                        if (forEntity.getStatusCode() == HttpStatus.CREATED) {
                            System.out.println("Success ----------- " + name.text());
                        } else {
                            System.out.println("FAIL ----------- " + name.text());
                        }

                    }

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private static void quantranviet(String token, String s, int cate, int brand) {

        restTemplate = new RestTemplate();
        List<String> urls = new ArrayList<>();
        urls.add(s);
        for (String url : urls
        ) {
            System.out.println("Fetching %s..." + url);
            try {
                Document doc = Jsoup.connect(url).get();
                Elements elements = doc.select("div .product-small.box");
                for (Element e : elements
                ) {
                    Element name = e.getElementsByClass("product-title").first();
                    Element disPrice = e.getElementsByClass("woocommerce-Price-amount").last();
                    Element link = e.select("a[href]").first(); // a with href
                    if (name != null && disPrice != null && disPrice.text() != "" && link != null) {
                        Document docDetail = Jsoup.connect(link.absUrl("href").replace("../", "")).get();
                        Element longdescription = docDetail.getElementsByClass("noi-san-xuat").first();
                        Element imageThumbnail = e.getElementsByClass("box-image").select("img").first();
                        String imageThumb = getImage(imageThumbnail.absUrl("src").replace("../", ""));
                        Elements images = docDetail.getElementsByClass("fotorama__nav--thumbs").select("img");
                        List<String> imagesName = new ArrayList<>();
                        if (images != null) {
                            for (Element ele : images) {
                                imagesName.add(getImage(ele.absUrl("src").replace("../", "")));
                            }
                        }
                        ProductModel productModel = new ProductModel();
                        // Product Dto
                        ProductDto productDto = new ProductDto();
                        productDto.setName(name.text());
                        productDto.setSku(null);
                        if (longdescription != null) {
                            productDto.setDescription(longdescription.text());
                        }


                        productDto.setProductCategoryId(new BigDecimal(cate));
                        productDto.setBrandId(new BigDecimal(brand));


                        productDto.setCurrencyCode("VND");
                        productDto.setPrice(new BigDecimal(getPrice(disPrice.text())));
                        productDto.setFomula(new BigDecimal(1));
                        productDto.setQty(new BigDecimal(10));
                        productDto.setWeight(new BigDecimal(1));
                        productDto.setShelfWidth(new BigDecimal(1));
                        productDto.setShelfDepth(new BigDecimal(1));
                        productDto.setShelfHeight(new BigDecimal(1));
                        productDto.setImageThumbnail(imageThumb);
                        productModel.setProductDto(productDto);
                        // Product Carrier
                        List<ProductCarrierDto> productCarrierDtos = new ArrayList<>();
                        ProductCarrierDto productCarrierDto1 = new ProductCarrierDto();

                        productCarrierDto1.setCarrierId(new BigDecimal(3));
                        productCarrierDto1.setCarrierName("Giao Hàng Tiết Kiệm");
                        productCarrierDtos.add(productCarrierDto1);


                        ProductCarrierDto productCarrierDto2 = new ProductCarrierDto();
                        productCarrierDto2.setCarrierId(new BigDecimal(1));
                        productCarrierDto2.setCarrierName("Giao Hàng EShip");
                        productCarrierDtos.add(productCarrierDto2);

                        ProductCarrierDto productCarrierDto3 = new ProductCarrierDto();
                        productCarrierDto3.setCarrierId(new BigDecimal(2));
                        productCarrierDto3.setCarrierName("Giao Hàng Nhanh");
                        productCarrierDtos.add(productCarrierDto3);

                        // Product Image
                        List<ProductImageDto> productImageDtos = new ArrayList<>();
                        for (int i = 0; i < imagesName.size(); i++) {
                            ProductImageDto productImageDto = new ProductImageDto();
                            productImageDto.setName(imagesName.get(i));
                            productImageDtos.add(productImageDto);
                        }


                        productModel.setProductDto(productDto);
                        productModel.setProductCarrierDtos(productCarrierDtos);
                        productModel.setProductImageDtos(productImageDtos);
                        HttpHeaders headers = new HttpHeaders();
                        headers.setContentType(MediaType.APPLICATION_JSON);
                        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
                        headers.set("ACCESS_TOKEN", "Bearer " + token);
                        HttpEntity<ProductModel> entity = new HttpEntity<>(productModel, headers);
                        ResponseEntity<ServiceResult> forEntity = restTemplate.postForEntity("http://103.226.248.230:8889/api/product/create", entity,
                                ServiceResult.class);
                        if (forEntity.getStatusCode() == HttpStatus.CREATED) {
                            System.out.println("Success ----------- " + name.text());
                        } else {
                            System.out.println("FAIL ----------- " + name.text());
                        }

                    }

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private static void goldSun(String token, String s, int cate, int brand) {

        restTemplate = new RestTemplate();
        List<String> urls = new ArrayList<>();
        urls.add(s);
        for (String url : urls
        ) {
            System.out.println("Fetching %s..." + url);
            try {
                Document doc = Jsoup.connect(url).get();
                Elements elements = doc.select("div .item-sp");
                for (Element e : elements
                ) {
                    Element name = e.getElementsByClass("info-sp").select("a").first();
                    Element link = e.select("a[href]").first(); // a with href
                    if (name != null && link != null) {
                        Document docDetail = Jsoup.connect(link.absUrl("href")).get();

                            Element longdescription = docDetail.getElementsByClass("info").first();
                            Element imageThumbnail = e.select("img").first();
                            String imageThumb = getImage(imageThumbnail.absUrl("data-src"));
                        if (docDetail.getElementsByClass("slide-sp").first() != null) {
                            Elements images = docDetail.getElementsByClass("slide-sp").first().select("img").not(".fancybox");
                            Element sku = docDetail.getElementsByClass("c-mm").select("span").first();
                            List<String> imagesName = new ArrayList<>();
                            if (images != null) {
                                for (Element ele : images) {
                                    imagesName.add(getImage(ele.absUrl("src")));
                                }
                            }
                            ProductModel productModel = new ProductModel();
                            // Product Dto
                            ProductDto productDto = new ProductDto();
                            productDto.setName(name.text());

                            if (longdescription != null) {
                                productDto.setDescription(longdescription.text());
                            }


                            productDto.setProductCategoryId(new BigDecimal(cate));
                            productDto.setBrandId(new BigDecimal(brand));

                            productDto.setSku(sku.text().replace("Mã SKU: ", "").trim());
                            productDto.setCurrencyCode("VND");
                            productDto.setPrice(BigDecimal.ZERO);
                            productDto.setFomula(new BigDecimal(1));
                            productDto.setQty(new BigDecimal(10));
                            productDto.setWeight(new BigDecimal(1));
                            productDto.setShelfWidth(new BigDecimal(1));
                            productDto.setShelfDepth(new BigDecimal(1));
                            productDto.setShelfHeight(new BigDecimal(1));
                            productDto.setImageThumbnail(imageThumb);
                            productModel.setProductDto(productDto);
                            // Product Carrier
                            List<ProductCarrierDto> productCarrierDtos = new ArrayList<>();
                            ProductCarrierDto productCarrierDto1 = new ProductCarrierDto();

                            productCarrierDto1.setCarrierId(new BigDecimal(3));
                            productCarrierDto1.setCarrierName("Giao Hàng Tiết Kiệm");
                            productCarrierDtos.add(productCarrierDto1);


                            ProductCarrierDto productCarrierDto2 = new ProductCarrierDto();
                            productCarrierDto2.setCarrierId(new BigDecimal(1));
                            productCarrierDto2.setCarrierName("Giao Hàng EShip");
                            productCarrierDtos.add(productCarrierDto2);

                            ProductCarrierDto productCarrierDto3 = new ProductCarrierDto();
                            productCarrierDto3.setCarrierId(new BigDecimal(2));
                            productCarrierDto3.setCarrierName("Giao Hàng Nhanh");
                            productCarrierDtos.add(productCarrierDto3);

                            // Product Image
                            List<ProductImageDto> productImageDtos = new ArrayList<>();
                            for (int i = 0; i < imagesName.size(); i++) {
                                ProductImageDto productImageDto = new ProductImageDto();
                                productImageDto.setName(imagesName.get(i));
                                productImageDtos.add(productImageDto);
                            }


                            productModel.setProductDto(productDto);
                            productModel.setProductCarrierDtos(productCarrierDtos);
                            productModel.setProductImageDtos(productImageDtos);
                            HttpHeaders headers = new HttpHeaders();
                            headers.setContentType(MediaType.APPLICATION_JSON);
                            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
                            headers.set("ACCESS_TOKEN", "Bearer " + token);
                            HttpEntity<ProductModel> entity = new HttpEntity<>(productModel, headers);
                            ResponseEntity<ServiceResult> forEntity = restTemplate.postForEntity("http://103.226.248.230:8889/api/product/create", entity,
                                    ServiceResult.class);
                            if (forEntity.getStatusCode() == HttpStatus.CREATED) {
                                System.out.println("Success ----------- " + name.text());
                            } else {
                                System.out.println("FAIL ----------- " + name.text());
                            }

                        }
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private static void sieuthigiadung(String token, String s, int cate, int brand) {

        restTemplate = new RestTemplate();
        List<String> urls = new ArrayList<>();
        urls.add(s);
        for (String url : urls
        ) {
            System.out.println("Fetching %s..." + url);
            try {
                Document doc = Jsoup.connect(url).get();
                Elements elements = doc.select("div .product-box");
                for (Element e : elements
                ) {
                    Element name = e.getElementsByClass("product-name").first();
                    Element disPrice = e.getElementsByClass("special-price").first();
                    Element link = e.select("a[href]").first(); // a with href
                    if (name != null && link != null) {
                        Document docDetail = Jsoup.connect(link.absUrl("href")).get();
                        Element longdescription = docDetail.getElementsByClass("product-well").first();
                        Element imageThumbnail = e.select("img").first();
                        String imageThumb = getImage(imageThumbnail.absUrl("src"));
                        Elements images = docDetail.getElementsByClass("owl-stage").first().select("img");

                        List<String> imagesName = new ArrayList<>();
                        if (images != null) {
                            for (Element ele : images) {
                                imagesName.add(getImage(ele.absUrl("src")));
                            }
                        }
                        ProductModel productModel = new ProductModel();
                        // Product Dto
                        ProductDto productDto = new ProductDto();
                        productDto.setName(name.text());
                        productDto.setSku(null);
                        if (longdescription != null) {
                            productDto.setDescription(longdescription.text());
                        }


                        productDto.setProductCategoryId(new BigDecimal(cate));
                        productDto.setBrandId(new BigDecimal(brand));


                        productDto.setCurrencyCode("VND");
                        productDto.setPrice(new BigDecimal(getPrice(disPrice.text())));
                        productDto.setFomula(new BigDecimal(1));
                        productDto.setQty(new BigDecimal(10));
                        productDto.setWeight(new BigDecimal(1));
                        productDto.setShelfWidth(new BigDecimal(1));
                        productDto.setShelfDepth(new BigDecimal(1));
                        productDto.setShelfHeight(new BigDecimal(1));
                        productDto.setImageThumbnail(imageThumb);
                        productModel.setProductDto(productDto);
                        // Product Carrier
                        List<ProductCarrierDto> productCarrierDtos = new ArrayList<>();
                        ProductCarrierDto productCarrierDto1 = new ProductCarrierDto();

                        productCarrierDto1.setCarrierId(new BigDecimal(3));
                        productCarrierDto1.setCarrierName("Giao Hàng Tiết Kiệm");
                        productCarrierDtos.add(productCarrierDto1);


                        ProductCarrierDto productCarrierDto2 = new ProductCarrierDto();
                        productCarrierDto2.setCarrierId(new BigDecimal(1));
                        productCarrierDto2.setCarrierName("Giao Hàng EShip");
                        productCarrierDtos.add(productCarrierDto2);

                        ProductCarrierDto productCarrierDto3 = new ProductCarrierDto();
                        productCarrierDto3.setCarrierId(new BigDecimal(2));
                        productCarrierDto3.setCarrierName("Giao Hàng Nhanh");
                        productCarrierDtos.add(productCarrierDto3);

                        // Product Image
                        List<ProductImageDto> productImageDtos = new ArrayList<>();
                        for (int i = 0; i < imagesName.size(); i++) {
                            ProductImageDto productImageDto = new ProductImageDto();
                            productImageDto.setName(imagesName.get(i));
                            productImageDtos.add(productImageDto);
                        }


                        productModel.setProductDto(productDto);
                        productModel.setProductCarrierDtos(productCarrierDtos);
                        productModel.setProductImageDtos(productImageDtos);
                        HttpHeaders headers = new HttpHeaders();
                        headers.setContentType(MediaType.APPLICATION_JSON);
                        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
                        headers.set("ACCESS_TOKEN", "Bearer " + token);
                        HttpEntity<ProductModel> entity = new HttpEntity<>(productModel, headers);
                        ResponseEntity<ServiceResult> forEntity = restTemplate.postForEntity("http://103.226.248.230:8889/api/product/create", entity,
                                ServiceResult.class);
                        if (forEntity.getStatusCode() == HttpStatus.CREATED) {
                            System.out.println("Success ----------- " + name.text());
                        } else {
                            System.out.println("FAIL ----------- " + name.text());
                        }

                    }

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private static void sieuthigiadungJS(String token, String s, int cate, int brand) {

        restTemplate = new RestTemplate();
        List<String> urls = new ArrayList<>();
        urls.add(s);
        for (String url : urls
        ) {
            System.out.println("Fetching %s..." + url);
            try {
                WebClient webClient = new WebClient(BrowserVersion.CHROME);
                webClient.getOptions().setJavaScriptEnabled(true); // enable javascript
                webClient.getOptions().setThrowExceptionOnScriptError(false); //even if there is error in js continue
                webClient.waitForBackgroundJavaScript(30000); // important! wait when javascript finishes rendering
                HtmlPage doc = webClient.getPage(url);

                doc.getElementById("tournamentTable");
              //  Document doc = Jsoup.connect(url).get();
                List<HtmlDivision> elements = doc.getByXPath("//div[@class='product-box']");
                for (HtmlDivision e : elements
                ) {
                    HtmlAnchor name = e.getFirstByXPath(".//a");
                    HtmlDivision disPrice
                            =  e.getFirstByXPath("//div[@class='special-price']");
                    HtmlAnchor link =  e.getFirstByXPath(".//a"); // a with href
                    if (name != null && link != null) {
                        WebClient webClientDetail = new WebClient(BrowserVersion.CHROME);
                        webClientDetail.getOptions().setJavaScriptEnabled(true); // enable javascript
                        webClientDetail.getOptions().setThrowExceptionOnScriptError(false); //even if there is error in js continue
                        webClientDetail.waitForBackgroundJavaScript(5000); // important! wait when javascript finishes rendering
                        HtmlPage docDetail = webClientDetail.getPage(link.getAttribute("href"));
                        //Document docDetail = Jsoup.connect(link.absUrl("href")).get();
                        DomElement longdescription = docDetail.getElementById("myTabContent");
                        HtmlElement imageThumbnail = e.getElementsByTagName("img").get(0);
                        String imageThumb = getImage(imageThumbnail.getAttribute("src"));
                        DomNodeList<HtmlElement> images = docDetail.getElementById("gallery_01").getElementsByTagName("img");

                        List<String> imagesName = new ArrayList<>();
                        if (images != null) {
                            for (HtmlElement ele : images) {
                                imagesName.add(getImage(ele.getAttribute("src")));
                            }
                        }
                        ProductModel productModel = new ProductModel();
                        // Product Dto
                        ProductDto productDto = new ProductDto();
                        productDto.setName(name.getTextContent());
                        productDto.setSku(null);
                        if (longdescription != null) {
                            productDto.setDescription(longdescription.asText());
                        }


                        productDto.setProductCategoryId(new BigDecimal(cate));
                        productDto.setBrandId(new BigDecimal(brand));


                        productDto.setCurrencyCode("VND");
                        productDto.setPrice(new BigDecimal(getPrice(disPrice.getTextContent())));
                        productDto.setFomula(new BigDecimal(1));
                        productDto.setQty(new BigDecimal(10));
                        productDto.setWeight(new BigDecimal(1));
                        productDto.setShelfWidth(new BigDecimal(1));
                        productDto.setShelfDepth(new BigDecimal(1));
                        productDto.setShelfHeight(new BigDecimal(1));
                        productDto.setImageThumbnail(imageThumb);
                        productModel.setProductDto(productDto);
                        // Product Carrier
                        List<ProductCarrierDto> productCarrierDtos = new ArrayList<>();
                        ProductCarrierDto productCarrierDto1 = new ProductCarrierDto();

                        productCarrierDto1.setCarrierId(new BigDecimal(3));
                        productCarrierDto1.setCarrierName("Giao Hàng Tiết Kiệm");
                        productCarrierDtos.add(productCarrierDto1);


                        ProductCarrierDto productCarrierDto2 = new ProductCarrierDto();
                        productCarrierDto2.setCarrierId(new BigDecimal(1));
                        productCarrierDto2.setCarrierName("Giao Hàng EShip");
                        productCarrierDtos.add(productCarrierDto2);

                        ProductCarrierDto productCarrierDto3 = new ProductCarrierDto();
                        productCarrierDto3.setCarrierId(new BigDecimal(2));
                        productCarrierDto3.setCarrierName("Giao Hàng Nhanh");
                        productCarrierDtos.add(productCarrierDto3);

                        // Product Image
                        List<ProductImageDto> productImageDtos = new ArrayList<>();
                        for (int i = 0; i < imagesName.size(); i++) {
                            ProductImageDto productImageDto = new ProductImageDto();
                            productImageDto.setName(imagesName.get(i));
                            productImageDtos.add(productImageDto);
                        }


                        productModel.setProductDto(productDto);
                        productModel.setProductCarrierDtos(productCarrierDtos);
                        productModel.setProductImageDtos(productImageDtos);
                        HttpHeaders headers = new HttpHeaders();
                        headers.setContentType(MediaType.APPLICATION_JSON);
                        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
                        headers.set("ACCESS_TOKEN", "Bearer " + token);
                        HttpEntity<ProductModel> entity = new HttpEntity<>(productModel, headers);
                        ResponseEntity<ServiceResult> forEntity = restTemplate.postForEntity("http://103.226.248.230:8889/api/product/create", entity,
                                ServiceResult.class);
                        if (forEntity.getStatusCode() == HttpStatus.CREATED) {
                            System.out.println("Success ----------- " + name.getTextContent());
                        } else {
                            System.out.println("FAIL ----------- " + name.getTextContent());
                        }

                    }

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private static void vinhtien(String token, String s, int cate, int brand) {

        restTemplate = new RestTemplate();
        List<String> urls = new ArrayList<>();
        urls.add(s);
        for (String url : urls
        ) {
            System.out.println("Fetching %s..." + url);
            try {
                Document doc = Jsoup.connect(url).get();
                Elements elements = doc.select("div .pbox_listfor");
                for (Element e : elements
                ) {
                    Element name = e.select("a").get(1);
                    Element price = e.select("span").first();
                    if (name != null && price != null) {
                        Element description = e.select("p").first();
                        Elements images = e.select("img");

//                        String imageThumb = getImage();
                        URI uriThumb = new URI(null, e.select("img").first().absUrl("src"), null);
                        String imageThumb = getImage(uriThumb.toASCIIString());

                        List<String> imagesName = new ArrayList<>();
                        for (Element ele : images) {
                            URI uri = new URI(null, ele.absUrl("src"), null);
                            String image = uri.toASCIIString();
                            imagesName.add(getImage(image));
                        }

                        ProductModel productModel = new ProductModel();
                        // Product Dto
                        ProductDto productDto = new ProductDto();
                        productDto.setName(name.text());
                        productDto.setSku(null);
                        productDto.setDescription(description.text());

                        productDto.setProductCategoryId(new BigDecimal(cate));
                        productDto.setBrandId(new BigDecimal(brand));


                        productDto.setCurrencyCode("VND");
                        if(price.text().equals("Giá: Liên hệ")) {
                            productDto.setPrice(null);
                        } else {
                            productDto.setPrice(new BigDecimal(getPrice(price.text())));
                        }
                        productDto.setFomula(new BigDecimal(1));
                        productDto.setQty(new BigDecimal(10));
                        productDto.setWeight(new BigDecimal(1));
                        productDto.setShelfWidth(new BigDecimal(1));
                        productDto.setShelfDepth(new BigDecimal(1));
                        productDto.setShelfHeight(new BigDecimal(1));
                        productDto.setImageThumbnail(imageThumb);
                        productModel.setProductDto(productDto);
                        // Product Carrier
                        List<ProductCarrierDto> productCarrierDtos = new ArrayList<>();
                        ProductCarrierDto productCarrierDto1 = new ProductCarrierDto();

                        productCarrierDto1.setCarrierId(new BigDecimal(3));
                        productCarrierDto1.setCarrierName("Giao Hàng Tiết Kiệm");
                        productCarrierDtos.add(productCarrierDto1);


                        ProductCarrierDto productCarrierDto2 = new ProductCarrierDto();
                        productCarrierDto2.setCarrierId(new BigDecimal(1));
                        productCarrierDto2.setCarrierName("Giao Hàng EShip");
                        productCarrierDtos.add(productCarrierDto2);

                        ProductCarrierDto productCarrierDto3 = new ProductCarrierDto();
                        productCarrierDto3.setCarrierId(new BigDecimal(2));
                        productCarrierDto3.setCarrierName("Giao Hàng Nhanh");
                        productCarrierDtos.add(productCarrierDto3);

                        // Product Image
                        List<ProductImageDto> productImageDtos = new ArrayList<>();
                        for (int i = 0; i < imagesName.size(); i++) {
                            ProductImageDto productImageDto = new ProductImageDto();
                            productImageDto.setName(imagesName.get(i));
                            productImageDtos.add(productImageDto);
//                            System.out.println(imagesName.get(i));
                        }


                        productModel.setProductDto(productDto);
                        productModel.setProductCarrierDtos(productCarrierDtos);
                        productModel.setProductImageDtos(productImageDtos);
                        HttpHeaders headers = new HttpHeaders();
                        headers.setContentType(MediaType.APPLICATION_JSON);
                        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
                        headers.set("ACCESS_TOKEN", "Bearer " + token);
                        HttpEntity<ProductModel> entity = new HttpEntity<>(productModel, headers);
                        ResponseEntity<ServiceResult> forEntity = restTemplate.postForEntity("http://192.168.1.12:8889/api/product/create", entity,
                                ServiceResult.class);
                        if (forEntity.getStatusCode() == HttpStatus.CREATED) {
                            System.out.println("Success ----------- " + name.text());
                        } else {
                            System.out.println("FAIL ----------- " + name.text());
                        }

                    }

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private static void bizbooks(String token, String s, int cate, int brand) {

        restTemplate = new RestTemplate();
        List<String> urls = new ArrayList<>();
        urls.add(s);
        for (String url : urls
        ) {
            System.out.println("Fetching %s..." + url);
            try {
                Document doc = Jsoup.connect(url).get();
                Elements elements = doc.select("div .product");
                for (Element e : elements
                ) {
                    Element name = e.getElementsByClass("name").first();
                    Element price = e.getElementsByClass("woocommerce-Price-amount").first();
                    Element disPrice = e.getElementsByClass("woocommerce-Price-amount").first();
                    Element link = e.select("a[href]").first(); // a with href
                    if (name != null && disPrice != null && link != null) {
                        Document docDetail = Jsoup.connect(link.absUrl("href")).get();
                        Element description = docDetail.getElementsByClass("product-short-description").first();
                        Element longdescription = docDetail.select("div #tab-description").first();
                        Element imageThumbnail = docDetail.getElementsByClass("attachment-full").first();
                        URI uriThumb = new URI(null, imageThumbnail.absUrl("data-src"), null);
                        String imageThumb = getImage(uriThumb.toASCIIString());
                        Elements images = docDetail.getElementsByClass("attachment-woocommerce_thumbnail");
                        List<String> imagesName = new ArrayList<>();
                        for (Element ele : images) {
                            URI uri = new URI(null, ele.absUrl("data-src"), null);
                            String image = uri.toASCIIString();
                            imagesName.add(getImage(image));
                        }

                        ProductModel productModel = new ProductModel();
                        // Product Dto
                        ProductDto productDto = new ProductDto();
                        productDto.setName(name.text());
                        productDto.setSku(null);
                        if (longdescription != null) {
                            productDto.setDescription(longdescription.toString());
                        }


                        productDto.setProductCategoryId(new BigDecimal(cate));
                        productDto.setBrandId(new BigDecimal(brand));


                        productDto.setCurrencyCode("VND");
                        productDto.setPrice(new BigDecimal(getPrice(disPrice.text())));
                        productDto.setFomula(new BigDecimal(1));
                        productDto.setQty(new BigDecimal(10));
                        productDto.setWeight(new BigDecimal(1));
                        productDto.setShelfWidth(new BigDecimal(1));
                        productDto.setShelfDepth(new BigDecimal(1));
                        productDto.setShelfHeight(new BigDecimal(1));
                        productDto.setImageThumbnail(imageThumb);
                        productModel.setProductDto(productDto);
                        // Product Carrier
                        List<ProductCarrierDto> productCarrierDtos = new ArrayList<>();
                        ProductCarrierDto productCarrierDto1 = new ProductCarrierDto();

                        productCarrierDto1.setCarrierId(new BigDecimal(3));
                        productCarrierDto1.setCarrierName("Giao Hàng Tiết Kiệm");
                        productCarrierDtos.add(productCarrierDto1);


                        ProductCarrierDto productCarrierDto2 = new ProductCarrierDto();
                        productCarrierDto2.setCarrierId(new BigDecimal(1));
                        productCarrierDto2.setCarrierName("Giao Hàng EShip");
                        productCarrierDtos.add(productCarrierDto2);

                        ProductCarrierDto productCarrierDto3 = new ProductCarrierDto();
                        productCarrierDto3.setCarrierId(new BigDecimal(2));
                        productCarrierDto3.setCarrierName("Giao Hàng Nhanh");
                        productCarrierDtos.add(productCarrierDto3);

                        // Product Image
                        List<ProductImageDto> productImageDtos = new ArrayList<>();
                        for (int i = 0; i < imagesName.size(); i++) {
                            ProductImageDto productImageDto = new ProductImageDto();
                            productImageDto.setName(imagesName.get(i));
                            productImageDtos.add(productImageDto);
                        }


                        productModel.setProductDto(productDto);
                        productModel.setProductCarrierDtos(productCarrierDtos);
                        productModel.setProductImageDtos(productImageDtos);
                        HttpHeaders headers = new HttpHeaders();
                        headers.setContentType(MediaType.APPLICATION_JSON);
                        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
                        headers.set("ACCESS_TOKEN", "Bearer " + token);
                        HttpEntity<ProductModel> entity = new HttpEntity<>(productModel, headers);
                        ResponseEntity<ServiceResult> forEntity = restTemplate.postForEntity("http://192.168.1.12:8889/api/product/create", entity,
                                ServiceResult.class);
                        if (forEntity.getStatusCode() == HttpStatus.CREATED) {
                            System.out.println("Success ----------- " + name.text());
                        } else {
                            System.out.println("FAIL ----------- " + name.text());
                        }

                    }

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
    private static void hdgviethan(String token, String s, int cate, int brand) {

        restTemplate = new RestTemplate();
        List<String> urls = new ArrayList<>();
        urls.add(s);
        for (String url : urls
        ) {
            System.out.println("Fetching %s..." + url);
            try {
                Document doc = Jsoup.connect(url).get();
                Elements elements = doc.select("div .item_pro");
                for (Element e : elements
                ) {
                    Element name = e.getElementsByClass("name_pro").first();
                    Element price = e.getElementsByClass("gia").first();
                    Element disPrice = price.select("i").first();
                    Element link = e.select("a[href]").first(); // a with href
                    if (name != null  && link != null) {
                        Document docDetail = Jsoup.connect(link.absUrl("href")).get();
                        Element description = docDetail.getElementsByClass("product-short-description").first();
                        Element longdescription = docDetail.select("div #chitiet").first();
                        Element des = docDetail.select("div .box_mota").first();
                        URI uriThumb = new URI(null, e.select("div .img_p").select("img").first().absUrl("src"), null);
                        String imageThumb = getImage(uriThumb.toASCIIString());
                        Elements images = docDetail.select("div .elevatezoom-gallery");
                        List<String> imagesName = new ArrayList<>();
                        for (Element ele : images) {
                            URI uri = new URI(null, ele.select("img").first().absUrl("src"), null);
                            String image = uri.toASCIIString();
                            imagesName.add(getImage(image));
                        }
                        ProductModel productModel = new ProductModel();
                        // Product Dto
                        ProductDto productDto = new ProductDto();
                        productDto.setName(name.text());
                        productDto.setSku(null);
                        if (longdescription != null) {
                            productDto.setDescription(des.toString() + longdescription.toString());
                        }


                        productDto.setProductCategoryId(new BigDecimal(cate));
                        productDto.setBrandId(new BigDecimal(brand));


                        productDto.setCurrencyCode("VND");
                        if(disPrice != null) {
                            productDto.setPrice(new BigDecimal(getPrice(disPrice.text())));
                        }
                        productDto.setFomula(new BigDecimal(1));
                        productDto.setQty(new BigDecimal(10));
                        productDto.setWeight(new BigDecimal(1));
                        productDto.setShelfWidth(new BigDecimal(1));
                        productDto.setShelfDepth(new BigDecimal(1));
                        productDto.setShelfHeight(new BigDecimal(1));
//                        productDto.setRefundPolicy();
                        productDto.setImageThumbnail(imageThumb);
                        productModel.setProductDto(productDto);
                        // Product Carrier
                        List<ProductCarrierDto> productCarrierDtos = new ArrayList<>();
                        ProductCarrierDto productCarrierDto1 = new ProductCarrierDto();

                        productCarrierDto1.setCarrierId(new BigDecimal(3));
                        productCarrierDto1.setCarrierName("Giao Hàng Tiết Kiệm");
                        productCarrierDtos.add(productCarrierDto1);


                        ProductCarrierDto productCarrierDto2 = new ProductCarrierDto();
                        productCarrierDto2.setCarrierId(new BigDecimal(1));
                        productCarrierDto2.setCarrierName("Giao Hàng EShip");
                        productCarrierDtos.add(productCarrierDto2);

                        ProductCarrierDto productCarrierDto3 = new ProductCarrierDto();
                        productCarrierDto3.setCarrierId(new BigDecimal(2));
                        productCarrierDto3.setCarrierName("Giao Hàng Nhanh");
                        productCarrierDtos.add(productCarrierDto3);

                        // Product Image
                        List<ProductImageDto> productImageDtos = new ArrayList<>();
                        for (int i = 0; i < imagesName.size(); i++) {
                            ProductImageDto productImageDto = new ProductImageDto();
                            productImageDto.setName(imagesName.get(i));
                            productImageDtos.add(productImageDto);
                        }


                        productModel.setProductDto(productDto);
                        productModel.setProductCarrierDtos(productCarrierDtos);
                        productModel.setProductImageDtos(productImageDtos);
                        HttpHeaders headers = new HttpHeaders();
                        headers.setContentType(MediaType.APPLICATION_JSON);
                        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
                        headers.set("ACCESS_TOKEN", "Bearer " + token);
                        HttpEntity<ProductModel> entity = new HttpEntity<>(productModel, headers);
                        ResponseEntity<ServiceResult> forEntity = restTemplate.postForEntity("http://192.168.1.12:8889/api/product/create", entity,
                                ServiceResult.class);
                        if (forEntity.getStatusCode() == HttpStatus.CREATED) {
                            System.out.println("Success ----------- " + name.text());
                        } else {
                            System.out.println("FAIL ----------- " + name.text());
                        }

                    }

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private static void dscsport(String token, String s, int cate, int brand) {

        restTemplate = new RestTemplate();
        List<String> urls = new ArrayList<>();
        urls.add(s);
        for (String url : urls
        ) {
            System.out.println("Fetching %s..." + url);
            try {
                Document doc = Jsoup.connect(url).get();
                Elements elements = doc.select("div .product-grid");
                for (Element e : elements
                ) {
                    Element name = e.getElementsByClass("title-product-ajax-cart").first();
                    Element price = e.getElementsByClass("price").first();
                    Element disPrice = price.select("del").first();
                    Element link = e.select("a[href]").first(); // a with href
                    URI uriThumb = new URI(null, e.getElementsByClass("primary-img").first().absUrl("data-original"), null);
                    String imageThumb = getImage(uriThumb.toASCIIString());
                    if (name != null  && link != null) {
                        Document docDetail = Jsoup.connect(link.absUrl("href")).get();
                        Element longdescription = docDetail.select("div #tabs-1").first();
                        Elements images = docDetail.select("div .thumb-img");
                        List<String> imagesName = new ArrayList<>();
                        for (Element ele : images) {
                            URI uri = new URI(null, ele.select("img").first().absUrl("src"), null);
                            String image = uri.toASCIIString();
                            imagesName.add(getImage(image));
                        }
                        ProductModel productModel = new ProductModel();
                        // Product Dto
                        ProductDto productDto = new ProductDto();
                        productDto.setName(name.text());
                        productDto.setSku(null);
                        if (longdescription != null) {
                            productDto.setDescription(longdescription.toString());
                        }


                        productDto.setProductCategoryId(new BigDecimal(cate));
                        productDto.setBrandId(new BigDecimal(brand));


                        productDto.setCurrencyCode("VND");
                        if(disPrice != null) {
                            productDto.setPrice(new BigDecimal(getPrice(disPrice.text())));
                        }
                        productDto.setFomula(new BigDecimal(1));
                        productDto.setQty(new BigDecimal(10));
                        productDto.setWeight(new BigDecimal(1));
                        productDto.setShelfWidth(new BigDecimal(1));
                        productDto.setShelfDepth(new BigDecimal(1));
                        productDto.setShelfHeight(new BigDecimal(1));
//                        productDto.setRefundPolicy();
                        productDto.setImageThumbnail(imageThumb);
                        productModel.setProductDto(productDto);
                        // Product Carrier
                        List<ProductCarrierDto> productCarrierDtos = new ArrayList<>();
                        ProductCarrierDto productCarrierDto1 = new ProductCarrierDto();

                        productCarrierDto1.setCarrierId(new BigDecimal(3));
                        productCarrierDto1.setCarrierName("Giao Hàng Tiết Kiệm");
                        productCarrierDtos.add(productCarrierDto1);


                        ProductCarrierDto productCarrierDto2 = new ProductCarrierDto();
                        productCarrierDto2.setCarrierId(new BigDecimal(1));
                        productCarrierDto2.setCarrierName("Giao Hàng EShip");
                        productCarrierDtos.add(productCarrierDto2);

                        ProductCarrierDto productCarrierDto3 = new ProductCarrierDto();
                        productCarrierDto3.setCarrierId(new BigDecimal(2));
                        productCarrierDto3.setCarrierName("Giao Hàng Nhanh");
                        productCarrierDtos.add(productCarrierDto3);

                        // Product Image
                        List<ProductImageDto> productImageDtos = new ArrayList<>();
                        for (int i = 0; i < imagesName.size(); i++) {
                            ProductImageDto productImageDto = new ProductImageDto();
                            productImageDto.setName(imagesName.get(i));
                            productImageDtos.add(productImageDto);
                        }


                        productModel.setProductDto(productDto);
                        productModel.setProductCarrierDtos(productCarrierDtos);
                        productModel.setProductImageDtos(productImageDtos);
                        HttpHeaders headers = new HttpHeaders();
                        headers.setContentType(MediaType.APPLICATION_JSON);
                        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
                        headers.set("ACCESS_TOKEN", "Bearer " + token);
                        HttpEntity<ProductModel> entity = new HttpEntity<>(productModel, headers);
                        ResponseEntity<ServiceResult> forEntity = restTemplate.postForEntity("http://192.168.1.12:8889/api/product/create", entity,
                                ServiceResult.class);
                        if (forEntity.getStatusCode() == HttpStatus.CREATED) {
                            System.out.println("Success ----------- " + name.text());
                        } else {
                            System.out.println("FAIL ----------- " + name.text());
                        }

                    }

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


  private static void phuminh(String token, String s, int cate, int brand) {


        restTemplate = new RestTemplate();
        List<String> urls = new ArrayList<>();
        urls.add(s);
        for (String url : urls
        ) {
            System.out.println("Fetching %s..." + url);
            try {
                Document doc = Jsoup.connect(url).get();
                Elements elements = doc.select("div .product");
                for (Element e : elements
                ) {
                    Element name = e.getElementsByClass("prod-name-home").first();
                    Element price = e.getElementsByClass("curent-price-home").first().select("b").first();
                    Element disPrice = e.getElementsByClass("curent-price-home").first().select("del").first();
                    Element link = e.select("a[href]").first(); // a with href
                    URI uriThumb = new URI(null, e.select("img").first().absUrl("src"), null);
//                    System.out.println(uriThumb);
                    String imageThumb = getImage(uriThumb.toASCIIString());
                    if (name != null  && link != null) {
                        Document docDetail = Jsoup.connect(link.absUrl("href")).get();
                        Element longdescription = docDetail.select("div .tab-pane").first();
                        Element sku = docDetail.select("div .min-width").first().select("div .row").first().select("label").last();
                        Elements images = docDetail.select("div .carousel-item");
                        List<String> imagesName = new ArrayList<>();
                        for (Element ele : images) {
                            URI uri = new URI(null, ele.select("img").first().absUrl("src"), null);
                            String image = uri.toASCIIString();
//                            System.out.println(image);
                            imagesName.add(getImage(image));
                        }
                        ProductModel productModel = new ProductModel();
                        // Product Dto
                        ProductDto productDto = new ProductDto();
                        productDto.setName(name.text());
                        productDto.setSku(sku.text());
                        if (longdescription != null) {
                            productDto.setDescription(longdescription.text());
                        }


                        productDto.setProductCategoryId(new BigDecimal(cate));
                        productDto.setBrandId(new BigDecimal(brand));


                        productDto.setCurrencyCode("VND");
                        if(disPrice != null){
                            productDto.setPrice(new BigDecimal(getPrice(disPrice.text())));
                        }else{
                            productDto.setPrice(new BigDecimal(getPrice(price.text())));
                        }
                        productDto.setFomula(new BigDecimal(1));
                        productDto.setQty(new BigDecimal(10));
                        productDto.setWeight(new BigDecimal(1));
                        productDto.setShelfWidth(new BigDecimal(1));
                        productDto.setShelfDepth(new BigDecimal(1));
                        productDto.setShelfHeight(new BigDecimal(1));
//                        productDto.setRefundPolicy();
                        productDto.setImageThumbnail(imageThumb);
                        productModel.setProductDto(productDto);
                        // Product Carrier
                        List<ProductCarrierDto> productCarrierDtos = new ArrayList<>();
                        ProductCarrierDto productCarrierDto1 = new ProductCarrierDto();

                        productCarrierDto1.setCarrierId(new BigDecimal(3));
                        productCarrierDto1.setCarrierName("Giao Hàng Tiết Kiệm");
                        productCarrierDtos.add(productCarrierDto1);


                        ProductCarrierDto productCarrierDto2 = new ProductCarrierDto();
                        productCarrierDto2.setCarrierId(new BigDecimal(1));
                        productCarrierDto2.setCarrierName("Giao Hàng EShip");
                        productCarrierDtos.add(productCarrierDto2);

                        ProductCarrierDto productCarrierDto3 = new ProductCarrierDto();
                        productCarrierDto3.setCarrierId(new BigDecimal(2));
                        productCarrierDto3.setCarrierName("Giao Hàng Nhanh");
                        productCarrierDtos.add(productCarrierDto3);

                        // Product Image
                        List<ProductImageDto> productImageDtos = new ArrayList<>();
                        for (int i = 0; i < imagesName.size(); i++) {
                            ProductImageDto productImageDto = new ProductImageDto();
                            productImageDto.setName(imagesName.get(i));
                            productImageDtos.add(productImageDto);
                        }


                        productModel.setProductDto(productDto);
                        productModel.setProductCarrierDtos(productCarrierDtos);
                        productModel.setProductImageDtos(productImageDtos);
                        HttpHeaders headers = new HttpHeaders();
                        headers.setContentType(MediaType.APPLICATION_JSON);
                        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
                        headers.set("ACCESS_TOKEN", "Bearer " + token);
                        HttpEntity<ProductModel> entity = new HttpEntity<>(productModel, headers);
                        ResponseEntity<ServiceResult> forEntity = restTemplate.postForEntity("http://192.168.1.12:8889/api/product/create", entity,
                                ServiceResult.class);
                        if (forEntity.getStatusCode() == HttpStatus.CREATED) {
                            System.out.println("Success ----------- " + name.text());
                        } else {
                            System.out.println("FAIL ----------- " + name.text());
                        }

                    }

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private static String getImage(String url) {
        String name = "";
        try {
            URL imageUrl = new URL(url);
            MultipartFile file = new ByteToMultipartFile(extractBytes(imageUrl));
            ObjectMapper objectMapper = new ObjectMapper();
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://192.168.1.12:8889/api/image/product/upload");
            LinkedMultiValueMap<String, Object> requestEntity = new LinkedMultiValueMap<>();


            FileSystemResource value = new FileSystemResource(convert(file));
            requestEntity.add("file", value);

            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            HttpEntity<LinkedMultiValueMap<String, Object>> httpEntity = new HttpEntity(requestEntity, headers);

            final ResponseEntity<String> responseEntity = restTemplate.postForEntity(builder.toUriString(), httpEntity,
                    String.class);
            if (responseEntity.getStatusCodeValue() == 200) {
                JsonNode forImage = objectMapper.readTree(Objects.requireNonNull(responseEntity.getBody()));
                name = objectMapper.readValue(forImage.path("data").toString(), String.class);
            }

        } catch (IOException e) {

        }
        return name;
    }

    public static byte[] extractBytes(URL image) throws IOException {
        BufferedImage originalImage = ImageIO.read(image);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(originalImage, "png", baos);
        byte[] imageInByte = baos.toByteArray();
        return imageInByte;
    }

    public static File convert(MultipartFile file) throws IOException {
        File convFile;
        if (file.getOriginalFilename() != null) {
            convFile = new File(file.getOriginalFilename());
        } else {
            convFile = new File("Pictures");
        }
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    public static long getPrice(String price) {
        String newPrice = price.replaceAll("[^0-9]", "");
        if (newPrice.trim().length() >0) {
            return Long.parseLong(newPrice);
        } else {
            return 0;
        }
    }

}