
import org.springframework.http.HttpStatus;

public class ServiceResult {

    /** The status. */
    private Status status = Status.SUCCESS;

    private HttpStatus httpStatus;

    private int statusCode;

    /** The message. */
    private String message;


    /** The data. */
    private Object data;
    /**
     * The Enum Status.
     */
    public enum Status {

        /** The success. */
        SUCCESS,
        /** The failed. */
        FAILED,
        /** The token fail. */
        TOKEN_FAIL;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
