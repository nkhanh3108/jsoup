import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductCarrierDto {
    private BigDecimal productCarrierId;
    private BigDecimal productId;
    private BigDecimal carrierId;
    private String carrierName;

    public BigDecimal getProductCarrierId() {
        return productCarrierId;
    }

    public void setProductCarrierId(BigDecimal productCarrierId) {
        this.productCarrierId = productCarrierId;
    }

    public BigDecimal getProductId() {
        return productId;
    }

    public void setProductId(BigDecimal productId) {
        this.productId = productId;
    }

    public BigDecimal getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(BigDecimal carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }
}