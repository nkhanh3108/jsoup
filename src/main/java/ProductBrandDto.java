import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductBrandDto {


    private BigDecimal productBrandId;
    private BigDecimal productCategoryId;
    private String description;
    private String brandName;
    private String productBrandUu;
    private String brandType;

    public ProductBrandDto() {
    }



    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProductBrandUu() {
        return productBrandUu;
    }

    public void setProductBrandUu(String productBrandUu) {
        this.productBrandUu = productBrandUu;
    }

    public String getBrandType() {
        return brandType;
    }

    public void setBrandType(String brandType) {
        this.brandType = brandType;
    }

    public BigDecimal getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(BigDecimal productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public BigDecimal getProductBrandId() {
        return productBrandId;
    }

    public void setProductBrandId(BigDecimal productBrandId) {
        this.productBrandId = productBrandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}