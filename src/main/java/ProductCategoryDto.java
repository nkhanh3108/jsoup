import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductCategoryDto {
    private BigDecimal productCategoryId;
    private String name;


    public BigDecimal getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(BigDecimal productCategoryId) {
        this.productCategoryId = productCategoryId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}