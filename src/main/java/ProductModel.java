import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductModel {
    ProductDto productDto;

    ProductCategoryDto productCategoryDto;

    ProductBrandDto productBrandDto;

    List<ProductCarrierDto> productCarrierDtos;

    List<ProductImageDto> productImageDtos;

    List<AttributeSetInstanceModel> attributeSetInstanceModels;


    public ProductModel() {
    }

    public List<ProductCarrierDto> getProductCarrierDtos() {
        return productCarrierDtos;
    }

    public void setProductCarrierDtos(List<ProductCarrierDto> productCarrierDtos) {
        this.productCarrierDtos = productCarrierDtos;
    }

    public ProductDto getProductDto() {
        return productDto;
    }

    public void setProductDto(ProductDto productDto) {
        this.productDto = productDto;
    }

    public ProductCategoryDto getProductCategoryDto() {
        return productCategoryDto;
    }

    public void setProductCategoryDto(ProductCategoryDto productCategoryDto) {
        this.productCategoryDto = productCategoryDto;
    }

    public List<ProductImageDto> getProductImageDtos() {
        return productImageDtos;
    }

    public void setProductImageDtos(List<ProductImageDto> productImageDtos) {
        this.productImageDtos = productImageDtos;
    }

    public List<AttributeSetInstanceModel> getAttributeSetInstanceModels() {
        return attributeSetInstanceModels;
    }

    public void setAttributeSetInstanceModels(List<AttributeSetInstanceModel> attributeSetInstanceModels) {
        this.attributeSetInstanceModels = attributeSetInstanceModels;
    }

    public ProductBrandDto getProductBrandDto() {
        return productBrandDto;
    }

    public void setProductBrandDto(ProductBrandDto productBrandDto) {
        this.productBrandDto = productBrandDto;
    }
}