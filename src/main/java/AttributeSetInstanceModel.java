import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AttributeSetInstanceModel {

    AttributeSetInstanceDto attributeSetInstanceDto;

    List<AttributeInstanceDto> attributeInstancesDtos;

    public AttributeSetInstanceDto getAttributeSetInstanceDto() {
        return attributeSetInstanceDto;
    }

    public void setAttributeSetInstanceDto(AttributeSetInstanceDto attributeSetInstanceDto) {
        this.attributeSetInstanceDto = attributeSetInstanceDto;
    }

    public List<AttributeInstanceDto> getAttributeInstancesDtos() {
        return attributeInstancesDtos;
    }

    public void setAttributeInstancesDtos(List<AttributeInstanceDto> attributeInstancesDto) {
        this.attributeInstancesDtos = attributeInstancesDto;
    }
}
