import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AttributeInstanceDto {

    private BigDecimal attributeInstanceId;
    private BigDecimal attributeId;
    private BigDecimal attributeSetInstanceId;
    private String attributeName;
    private String description;
    private String name;

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public BigDecimal getAttributeInstanceId() {
        return attributeInstanceId;
    }

    public void setAttributeInstanceId(BigDecimal attributeInstanceId) {
        this.attributeInstanceId = attributeInstanceId;
    }

    public BigDecimal getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(BigDecimal attributeId) {
        this.attributeId = attributeId;
    }

    public BigDecimal getAttributeSetInstanceId() {
        return attributeSetInstanceId;
    }

    public void setAttributeSetInstanceId(BigDecimal attributeSetInstanceId) {
        this.attributeSetInstanceId = attributeSetInstanceId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

