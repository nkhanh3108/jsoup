import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AttributeSetInstanceDto {
    private BigDecimal attributeSetInstanceId;
    private BigDecimal productId;
    private BigDecimal attributeSetId;
    private String description;
    private String name;
    private BigDecimal qty;
    private BigDecimal price;
    private String imageThumbnail;
    private BigDecimal priceNew;


    public String getImageThumbnail() {
        return imageThumbnail;
    }

    public void setImageThumbnail(String imageThumbnail) {
        this.imageThumbnail = imageThumbnail;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getAttributeSetInstanceId() {
        return attributeSetInstanceId;
    }

    public void setAttributeSetInstanceId(BigDecimal attributeSetInstanceId) {
        this.attributeSetInstanceId = attributeSetInstanceId;
    }

    public BigDecimal getAttributeSetId() {
        return attributeSetId;
    }

    public void setAttributeSetId(BigDecimal attributeSetId) {
        this.attributeSetId = attributeSetId;
    }

    public BigDecimal getProductId() {
        return productId;
    }

    public void setProductId(BigDecimal productId) {
        this.productId = productId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPriceNew() {
        return priceNew;
    }

    public void setPriceNew(BigDecimal priceNew) {
        this.priceNew = priceNew;
    }
}

